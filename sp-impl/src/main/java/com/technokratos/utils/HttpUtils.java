package com.technokratos.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

@UtilityClass
public class HttpUtils {


    public static void sendException(HttpServletResponse response, Exception e, int statusCode) throws IOException {
        Map<String, Object> body = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(statusCode);
        body.put("status", statusCode);
        body.put("message", e.getMessage());

        mapper.writeValue(response.getOutputStream(), body);

    }

}
