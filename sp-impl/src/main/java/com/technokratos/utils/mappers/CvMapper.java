package com.technokratos.utils.mappers;

import com.technokratos.dto.CvDto;
import com.technokratos.model.Cv;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CvMapper {
    @Mapping(target = "cvFileId", source = "file.id")
    CvDto toResponse(Cv cv);
}
