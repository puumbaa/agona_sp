package com.technokratos.utils.mappers;

import com.technokratos.dto.response.FileInfoBriefResponse;
import com.technokratos.dto.response.FileInfoResponse;
import com.technokratos.model.FileInfo;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface FileMapper {

    @Mapping(target = "size", source = "fileInfo.metaInfo.size")
    @Mapping(target = "mimeType", source = "fileInfo.metaInfo.mimeType")
    @Mapping(target = "originalFileName", source = "fileInfo.metaInfo.originalFileName")
    FileInfoResponse toResponse(FileInfo fileInfo);

    @Named("toBriefResponse")
    FileInfoBriefResponse toBriefResponse(FileInfo fileInfo);

    @IterableMapping(qualifiedByName = "toBriefResponse")
    List<FileInfoBriefResponse> toBriefResponse(List<FileInfo> fileInfo);
}
