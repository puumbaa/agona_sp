package com.technokratos.utils.mappers;

import com.technokratos.dto.response.CourseResponse;
import com.technokratos.model.Course;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CourseMapper {
    CourseResponse toResponse(Course course);
    List<CourseResponse> toResponse(List<Course> courses);
}
