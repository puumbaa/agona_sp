package com.technokratos.utils.mappers;

import com.technokratos.dto.response.UniversityResponse;
import com.technokratos.model.University;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UniversityMapper {

    UniversityResponse toResponse(University university);
    List<UniversityResponse> toResponse(List<University> university);

}
