package com.technokratos.utils.mappers;

import com.technokratos.dto.response.AcademyGroupResponse;
import com.technokratos.model.AcademyGroup;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface AcademyGroupMapper {

    AcademyGroupResponse toResponse(AcademyGroup academyGroup);
    List<AcademyGroupResponse> toResponse(List<AcademyGroup> academyGroup);

}
