package com.technokratos.utils.mappers;

import com.technokratos.dto.response.UserResponse;
import com.technokratos.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, uses = {AcademyGroupMapper.class,
        UniversityMapper.class, CourseMapper.class, CvMapper.class})
public interface UserMapper {

    @Mapping(target = "university", source = "user.academyGroup.university")
    UserResponse toResponse(User user);

}
