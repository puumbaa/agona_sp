package com.technokratos.utils.mappers;

import com.technokratos.dto.response.PracticalWorkBriefResponse;
import com.technokratos.dto.response.PracticalWorkResponse;
import com.technokratos.model.PracticalWork;
import org.mapstruct.*;

import java.util.List;

@Mapper(uses = FileMapper.class, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PracticalWorkMapper {

    @IterableMapping(qualifiedByName = "toBriefResponse")
    List<PracticalWorkBriefResponse> toBriefResponse(List<PracticalWork> work);
    @Named("toBriefResponse")
    PracticalWorkBriefResponse toBriefResponse(PracticalWork work);
    PracticalWorkResponse toResponse(PracticalWork work);
}
