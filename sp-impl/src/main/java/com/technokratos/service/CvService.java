package com.technokratos.service;

import com.technokratos.dto.CvDto;
import com.technokratos.model.Cv;

public interface CvService {
    Cv update(Cv old, CvDto updated);

    CvDto toResponse(Cv cv);

}
