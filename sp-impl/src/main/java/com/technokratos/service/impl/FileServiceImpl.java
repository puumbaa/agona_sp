package com.technokratos.service.impl;

import com.mongodb.client.gridfs.model.GridFSFile;
import com.technokratos.dto.enums.FileType;
import com.technokratos.exceptions.FileDownloadException;
import com.technokratos.exceptions.FileUploadException;
import com.technokratos.exceptions.not_found.FileInformationNotFoundException;
import com.technokratos.model.FileInfo;
import com.technokratos.model.embeddable.FileMetaInfo;
import com.technokratos.repository.FileInfoRepository;
import com.technokratos.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.UUID;


@Slf4j
@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final GridFsTemplate gridFsTemplate;
    private final FileInfoRepository fileInfoRepository;

    @Override
    public FileInfo getFileInfoById(UUID id) {
        return fileInfoRepository
                .findById(id)
                .orElseThrow(FileInformationNotFoundException::new);
    }

    @Transactional
    @Override
    public void deleteById(UUID id) {
        FileInfo fileInfo = getFileInfoById(id);
        String storageId = fileInfo.getMetaInfo().getStorageId();
        gridFsTemplate.delete(new Query(Criteria.where("_id").is(storageId)));
        fileInfoRepository.delete(fileInfo);
    }


    @Override
    public Resource download(UUID uuid) {
        String storageId = getFileInfoById(uuid).getMetaInfo().getStorageId();

        GridFSFile file = gridFsTemplate.findOne(
                new Query(Criteria.where("_id").is(storageId))
        );

        try {
            return new InputStreamResource(
                    gridFsTemplate
                            .getResource(Objects.requireNonNull(file))
                            .getInputStream()
            );

        } catch (IOException e) {
            throw new FileDownloadException();
        }
    }

    @Transactional
    @Override
    public UUID upload(MultipartFile file, FileType type) {

        long size = file.getSize();
        String mimeType = file.getContentType();
        String originalFilename = file.getOriginalFilename();

        log.info("Upload file {} ...", originalFilename);
        try (InputStream in = file.getInputStream()) {
            String id = gridFsTemplate.store(in, originalFilename, file.getContentType()).toHexString();

            FileMetaInfo metaInfo = FileMetaInfo.builder()
                    .size(size)
                    .mimeType(mimeType)
                    .originalFileName(originalFilename)
                    .storageId(id)
                    .build();

            FileInfo fileInfo = FileInfo.builder()
                    .fileType(type)
                    .metaInfo(metaInfo)
                    .build();

            FileInfo savedFile = fileInfoRepository.save(fileInfo);
            log.info("file - {} uploaded!", originalFilename);
            return savedFile.getId();
        } catch (IOException e) {
            log.error("Can not read file - {}", originalFilename);
            throw new FileUploadException();
        }
    }

}
