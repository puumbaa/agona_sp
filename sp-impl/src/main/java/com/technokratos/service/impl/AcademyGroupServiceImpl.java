package com.technokratos.service.impl;

import com.technokratos.dto.enums.UniversityYear;
import com.technokratos.dto.response.AcademyGroupResponse;
import com.technokratos.exceptions.AcademyGroupNotFoundException;
import com.technokratos.model.AcademyGroup;
import com.technokratos.repository.AcademyGroupRepository;
import com.technokratos.service.AcademyGroupService;
import com.technokratos.utils.mappers.AcademyGroupMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AcademyGroupServiceImpl implements AcademyGroupService {
    private final AcademyGroupRepository academyGroupRepository;
    private final AcademyGroupMapper academyGroupMapper;

    @Override
    public AcademyGroup findById(UUID id) {
        return getAcademyGroup(id);
    }

    @Override
    public List<AcademyGroupResponse> search(UUID universityId, UniversityYear universityYear, String query) {
        if (query == null) {
            query = "";
        }
        return academyGroupRepository.findAllByUniversity_IdAndUniversityYearAndNameContainingIgnoreCase(
                universityId, universityYear,
                query,
                PageRequest.of(0, 10))
                .map(academyGroupMapper::toResponse)
                .toList();
    }


    private AcademyGroup getAcademyGroup(UUID id) {
        return academyGroupRepository
                .findById(id)
                .orElseThrow(AcademyGroupNotFoundException::new);
    }
}
