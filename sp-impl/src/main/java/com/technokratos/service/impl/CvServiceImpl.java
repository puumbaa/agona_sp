package com.technokratos.service.impl;

import com.technokratos.dto.CvDto;
import com.technokratos.dto.enums.FileType;
import com.technokratos.exceptions.SkillNotFoundException;
import com.technokratos.exceptions.files.WrongFileTypeException;
import com.technokratos.model.Cv;
import com.technokratos.model.FileInfo;
import com.technokratos.repository.CvRepository;
import com.technokratos.repository.SkillRepository;
import com.technokratos.service.CvService;
import com.technokratos.service.FileService;
import com.technokratos.utils.mappers.CvMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CvServiceImpl implements CvService {
    private final CvRepository cvRepository;
    private final SkillRepository skillRepository;
    private final FileService fileService;
    private final CvMapper cvMapper;


    @Override
    public Cv update(Cv old, CvDto updated) {
        FileInfo cvFile = fileService.getFileInfoById(updated.getCvFileId());
        if (!cvFile.getFileType().equals(FileType.USER_CV)) {
            throw new WrongFileTypeException(FileType.USER_CV, cvFile.getFileType());
        }
        if (old == null){
            old = new Cv();
        }else{
            fileService.deleteById(old.getId());
        }
        old.setFile(cvFile);
        old.setExperienceDescription(updated.getExperienceDescription());
        old.setPortfolioLinks(updated.getPortfolioLinks());
        old.setSkills(updated.getSkills()
                .stream()
                .map(skillDto -> skillRepository
                        .findSkillByName(skillDto.getName())
                        .orElseThrow(SkillNotFoundException::new))
                .collect(Collectors.toSet()));

        return cvRepository.save(old);
    }

    @Override
    public CvDto toResponse(Cv cv) {
        return cvMapper.toResponse(cv);
    }
}
