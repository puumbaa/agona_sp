package com.technokratos.service.impl;

import com.technokratos.dto.request.RegistrationStartRequest;
import com.technokratos.dto.request.RegistrationCompleteRequest;
import com.technokratos.dto.enums.State;
import com.technokratos.exceptions.EmailAlreadyExistsException;
import com.technokratos.model.Confirmation;
import com.technokratos.model.User;
import com.technokratos.repository.UserRepository;
import com.technokratos.service.ConfirmationService;
import com.technokratos.service.MailService;
import com.technokratos.service.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static com.technokratos.dto.enums.Role.USER;
import static com.technokratos.dto.enums.State.CONFIRMED;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final MailService mailService;
    private final ConfirmationService confirmationService;

    @Value("${registration.completion.url}")
    private String registrationCompletionUrl;


    @Transactional
    @Override
    public String startRegistration(RegistrationStartRequest data) {
        if (userRepository.existsByEmail(data.getEmail())) {
            throw new EmailAlreadyExistsException();
        }

        User user = User.builder()
                .firstName(data.getFirstName())
                .lastName(data.getLastName())
                .email(data.getEmail())
                .state(State.NOT_CONFIRMED)
                .build();

        userRepository.save(user);
        Confirmation confirmation = confirmationService.createForUser(user);
        Map<String, String> model = Map.of(
                "url", registrationCompletionUrl,
                "code", confirmation.getId().toString());

        mailService.sendMail("Registration completion", data.getEmail(), model);

        return String.format("An email with a link to complete registration has been sent to %s", data.getEmail());
    }


    @Transactional
    @Override
    public String completeRegistration(RegistrationCompleteRequest data, String code) {
        String password = data.getPassword();
        String passwordConfirm = data.getPasswordConfirmation();

        if (!password.equals(passwordConfirm)) {
            throw new BadCredentialsException("Passwords must match");
        }

        Confirmation confirmation = confirmationService.getConfirmation(code);

        User user = confirmation.getUser();

        user.setRole(USER);
        user.setPassword(passwordEncoder.encode(password));
        user.setState(CONFIRMED);

        confirmationService.delete(confirmation);
        userRepository.save(user);

        return String.format("Account - %s confirmed successfully", user.getEmail());
    }
}
