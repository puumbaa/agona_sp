package com.technokratos.service.impl;

import com.technokratos.exceptions.not_found.ConfirmationCodeNotFoundException;
import com.technokratos.model.Confirmation;
import com.technokratos.model.User;
import com.technokratos.repository.ConfirmationRepository;
import com.technokratos.service.ConfirmationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class ConfirmationServiceImpl implements ConfirmationService {

    private final ConfirmationRepository confirmationRepository;

    @Override
    public Confirmation createForUser(User user) {
        return confirmationRepository.save(Confirmation.builder()
                .user(user)
                .build());
    }


    @Override
    public Confirmation getConfirmation(String code) {
        return confirmationRepository
                .findById(UUID.fromString(code))
                .orElseThrow(ConfirmationCodeNotFoundException::new);
    }

    @Override
    public void delete(Confirmation confirmation) {
        confirmationRepository.delete(confirmation);
    }

}
