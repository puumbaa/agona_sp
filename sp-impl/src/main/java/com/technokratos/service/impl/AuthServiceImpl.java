package com.technokratos.service.impl;

import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.response.TokenCoupleResponse;
import com.technokratos.exceptions.BadCredentialsException;
import com.technokratos.model.User;
import com.technokratos.repository.UserRepository;
import com.technokratos.service.AuthService;
import com.technokratos.service.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.technokratos.dto.enums.State.CONFIRMED;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final UserRepository userRepository;
    private final Environment environment;


    @Override
    public TokenCoupleResponse login(LoginRequest data) {
        User user = userRepository.findByEmail(data.getEmail())
                .filter(user1 -> user1.getState().equals(CONFIRMED))
                .filter(account -> passwordEncoder.matches(data.getPassword(), account.getPassword()))
                .orElseThrow(() -> new BadCredentialsException("Wrong email or password!"));
        System.out.println(user.toString());
        return jwtService.generateTokenCouple(user);
    }


}
