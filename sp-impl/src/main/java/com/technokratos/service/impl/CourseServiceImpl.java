package com.technokratos.service.impl;

import com.technokratos.dto.response.CourseResponse;
import com.technokratos.exceptions.AcademyGroupNotFoundException;
import com.technokratos.exceptions.not_found.CourseNotFoundException;
import com.technokratos.exceptions.not_found.UserNotFoundException;
import com.technokratos.model.AcademyGroup;
import com.technokratos.model.Course;
import com.technokratos.model.University;
import com.technokratos.model.User;
import com.technokratos.repository.CourseRepository;
import com.technokratos.repository.UserRepository;
import com.technokratos.service.CourseService;
import com.technokratos.utils.mappers.CourseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;
    private final UserRepository userRepository;
    private final CourseMapper courseMapper;

    @Override
    public List<CourseResponse> findAll() {
        return courseMapper.toResponse(courseRepository.findAll());

    }

    @Override
    public CourseResponse findById(UUID id) {
        return courseMapper.toResponse(
                courseRepository
                        .findById(id)
                        .orElseThrow(CourseNotFoundException::new)
        );
    }

    @Override
    public List<CourseResponse> getAvailableCourses(UUID userId) {
        User user = userRepository
                .findById(userId)
                .orElseThrow(UserNotFoundException::new);

        AcademyGroup academyGroup = user.getAcademyGroup();

        if (academyGroup == null) {
            return Collections.emptyList();
        }

        University university = academyGroup.getUniversity();
        List<Course> byUniversity = courseRepository.findByUniversity(university);
        return courseMapper.toResponse(byUniversity);
    }
}
