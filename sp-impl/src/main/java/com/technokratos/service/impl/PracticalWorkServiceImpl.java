package com.technokratos.service.impl;

import com.technokratos.dto.response.PracticalWorkBriefResponse;
import com.technokratos.dto.response.PracticalWorkResponse;
import com.technokratos.exceptions.PracticalWorkNotFoundException;
import com.technokratos.repository.PracticalWorkRepository;
import com.technokratos.service.PracticalWorkService;
import com.technokratos.utils.mappers.PracticalWorkMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class PracticalWorkServiceImpl implements PracticalWorkService {
    private final PracticalWorkRepository practicalWorkRepository;
    private final PracticalWorkMapper practicalWorkMapper;

    @Override
    public List<PracticalWorkBriefResponse> findAllByCourseId(UUID courseId) {
        return practicalWorkMapper.toBriefResponse(practicalWorkRepository.findByCourse_Id(courseId));
    }

    @Override
    public PracticalWorkResponse findById(UUID id) {
        return practicalWorkMapper.toResponse(practicalWorkRepository.findById(id).orElseThrow(PracticalWorkNotFoundException::new));
    }
}
