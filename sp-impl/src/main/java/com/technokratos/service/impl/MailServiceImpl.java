package com.technokratos.service.impl;

import com.technokratos.service.MailService;
import com.technokratos.service.TemplateProcessor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.util.Map;

// todo: take out the template processing logic

@Slf4j
@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {
    private final JavaMailSender mailSender;
    private final TemplateProcessor templateProcessor;

    @Value("${spring.mail.username}")
    private String from;


    public void sendMail(String subject, String to, Map<String, String> model) {
        MessageDto message = MessageDto.builder()
                .subject(subject)
                .to(to)
                .model(model)
                .build();

        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "UTF-8");
            helper.setSubject(message.subject);
            helper.setText(templateProcessor.process(model), true);
            helper.setFrom(from);
            helper.setTo(message.to);
        };
        mailSender.send(preparator);
    }


    @Builder
    private static class MessageDto {
        private final String subject;
        private final String to;
        private final Map<String, String> model;
    }

}
