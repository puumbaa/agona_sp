package com.technokratos.service.impl;

import com.technokratos.service.TemplateProcessor;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class TemplateProcessorImpl implements TemplateProcessor {

    private final FreeMarkerConfigurer freeMarkerConfigurer;
    @Value("${template-processor.template-name}")
    private String templateName;


    @Override
    public String process( Map<String, String> model) {
        try {
            Template template = freeMarkerConfigurer.getConfiguration().getTemplate(templateName);
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        }catch (TemplateNotFoundException e) {
            log.warn("Template not found");
            throw new IllegalArgumentException("Email template not found");
        } catch (TemplateException | IOException e) {
           log.error("Template processing exception");
           throw new IllegalArgumentException("Error during processing template");
        }
    }
}
