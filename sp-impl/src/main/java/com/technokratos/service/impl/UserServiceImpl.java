package com.technokratos.service.impl;

import com.technokratos.dto.*;
import com.technokratos.dto.enums.FileType;
import com.technokratos.dto.request.EditAcademyGroupRequest;
import com.technokratos.dto.request.EditUserPasswordRequest;
import com.technokratos.dto.request.EditUserPhotoRequest;
import com.technokratos.dto.request.EditUserRequest;
import com.technokratos.dto.response.EditAcademyGroupResponse;
import com.technokratos.dto.response.UserResponse;
import com.technokratos.exceptions.WrongOldPasswordException;
import com.technokratos.exceptions.files.WrongFileTypeException;
import com.technokratos.exceptions.not_found.UserNotFoundException;
import com.technokratos.model.FileInfo;
import com.technokratos.model.User;
import com.technokratos.repository.UserRepository;
import com.technokratos.service.AcademyGroupService;
import com.technokratos.service.CvService;
import com.technokratos.service.FileService;
import com.technokratos.service.UserService;
import com.technokratos.utils.mappers.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.technokratos.dto.enums.FileType.USER_PHOTO;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final FileService fileService;
    private final AcademyGroupService academyGroupService;
    private final PasswordEncoder passwordEncoder;
    private final CvService cvService;


    @Override
    public UserResponse getById(UUID id) {
        return userMapper.toResponse(getUser(id));
    }

    @Transactional
    @Override
    public void setPhoto(EditUserPhotoRequest photo, UUID userId) {
        User user = getUser(userId);
        FileInfo fileInfo = fileService.getFileInfoById(photo.getUserPhotoId());
        FileType fileType = fileInfo.getFileType();
        if (!fileType.equals(USER_PHOTO)) {
            throw new WrongFileTypeException(USER_PHOTO, fileType);
        }
        if (user.getPhoto() != null){
            fileService.deleteById(user.getPhoto().getId());
        }
        user.setPhoto(fileInfo);
        userRepository.save(user);
    }

    @Transactional
    @Override
    public Resource getPhoto(UUID userId) {
        User user = getUser(userId);
        UUID photo = user.getPhoto().getId();
        return fileService.download(photo);

    }


    @Override
    public UserResponse updateUser(UUID id, EditUserRequest updated) {
        User user = getUser(id);
        user.setFirstName(updated.getFirstName());
        user.setLastName(updated.getLastName());
        user.setPhone(updated.getPhone());
        user.setBirthdate(updated.getBirthdate());
        user.setAddress(updated.getAddress());
        user.setTelegram(updated.getTelegram());
        return userMapper.toResponse(userRepository.save(user));
    }

    @Transactional
    @Override
    public EditAcademyGroupResponse updateAcademyGroup(UUID userId, EditAcademyGroupRequest academyGroup) {
        User user = getUser(userId);
        user.setAcademyGroup(academyGroupService.findById(academyGroup.getAcademyGroup()));
        userRepository.save(user);
        return EditAcademyGroupResponse
                .builder()
                .academyGroup(academyGroup.getAcademyGroup())
                .build();
    }

    @Transactional
    @Override
    public String updatePassword(UUID id, EditUserPasswordRequest updated) {
        User user = getUser(id);
        if (!passwordEncoder.matches(updated.getOldPassword(), user.getPassword())) {
            throw new WrongOldPasswordException();
        }
        user.setPassword(passwordEncoder.encode(updated.getNewPassword()));
        userRepository.save(user);
        return "Password updated successfully";
    }

    @Override
    public CvDto updateUserCv(UUID id, CvDto updated) {
        User user = getUser(id);
        user.setCv(cvService.update(user.getCv(), updated));
        return cvService.toResponse(userRepository.save(user).getCv());
    }



    private User getUser(UUID userId) {
        return userRepository
                .findById(userId)
                .orElseThrow(UserNotFoundException::new);
    }
}
