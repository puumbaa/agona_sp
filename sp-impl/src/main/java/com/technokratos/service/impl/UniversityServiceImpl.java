package com.technokratos.service.impl;

import com.technokratos.dto.response.UniversityResponse;
import com.technokratos.repository.UniversityRepository;
import com.technokratos.service.UniversityService;
import com.technokratos.utils.mappers.UniversityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UniversityServiceImpl implements UniversityService {

    private final UniversityRepository universityRepository;
    private final UniversityMapper universityMapper;
    private final EntityManager em;

    @Override
    public List<UniversityResponse> search(String query) {
        if (query == null) {
            query = "";
        }
        return universityRepository
                .findAllByNameContainingIgnoreCase(query, PageRequest.of(0, 20))
                .map(universityMapper::toResponse)
                .toList();
    }


}
