package com.technokratos.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.technokratos.dto.response.TokenCoupleResponse;
import com.technokratos.dto.enums.Role;
import com.technokratos.dto.enums.State;
import com.technokratos.exceptions.jwt.InvalidRefreshTokenException;
import com.technokratos.exceptions.jwt.RefreshTokenExpiredException;
import com.technokratos.exceptions.jwt.WrongTokenFormatException;
import com.technokratos.model.RefreshToken;
import com.technokratos.model.User;
import com.technokratos.repository.RefreshTokenRepository;
import com.technokratos.security.details.UserDetailsImpl;
import com.technokratos.service.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService {

    public static final String BEARER = "Bearer ";


    private final RefreshTokenRepository refreshTokenRepository;

    @Value("${token.secret-key}")
    private String secretKey;

    @Value("${token.refresh.expiration-in-days}")
    private Long refreshTokenExpiration;

    @Value("${token.access.expiration-in-millis}")
    private Long accessTokenExpiration;


    public String getTokenFromAuthorizationHeader(String header) {
        if (header == null) {
            log.warn("Authorization header is null");
            return null;
        }

        if (!header.startsWith(BEARER)) {
            throw new WrongTokenFormatException();
        }
        return header.substring(BEARER.length()).trim();
    }


    private DecodedJWT decodeJWT(String token) {
        return JWT.require(Algorithm.HMAC256(secretKey))
                .build()
                .verify(token);
    }

    @Override
    public UserDetails getUserByToken(String token) {
        DecodedJWT decoded = decodeJWT(token);
        return UserDetailsImpl.builder()
                .email(decoded.getSubject())
                .role(Role.valueOf(decoded.getClaim("role").asString()))
                .state(State.valueOf(decoded.getClaim("state").asString()))
                .build();
    }

    @Override
    public String generateAccessToken(User user) {
        return JWT.create()
                .withSubject(user.getEmail())
                .withClaim("role", user.getRole().name())
                .withClaim("state", user.getState().name())
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + accessTokenExpiration))
                .sign(Algorithm.HMAC256(secretKey));
    }

    @Override
    public UUID generateRefreshToken(User user) {
        RefreshToken token = refreshTokenRepository
                .findByUser(user)
                .orElseGet(() -> RefreshToken.builder().user(user).build());

        token.setExpiredAt(LocalDateTime.now().plusDays(refreshTokenExpiration));
        return refreshTokenRepository.save(token).getId();

    }

    @Override
    public TokenCoupleResponse generateTokenCouple(User user) {
        return TokenCoupleResponse.builder()
                .accessToken(generateAccessToken(user))
                .refreshToken(generateRefreshToken(user))
                .build();
    }

    @Override
    public RefreshToken getRefreshToken(UUID refreshToken) {
        return refreshTokenRepository
                .findById(refreshToken)
                .orElseThrow(InvalidRefreshTokenException::new);
    }

    @Override
    public void deleteRefreshToken(UUID refreshToken) {
        refreshTokenRepository.deleteById(refreshToken);
    }

    @Override
    public TokenCoupleResponse refresh(UUID refreshToken) {
        RefreshToken refresh = getRefreshToken(refreshToken);

        if (LocalDateTime.now().isAfter(refresh.getExpiredAt())) {
            log.debug("token - {} has expired", refreshToken);
            deleteRefreshToken(refreshToken);
            throw new RefreshTokenExpiredException();
        }

        return TokenCoupleResponse.builder()
                .accessToken(generateAccessToken(refresh.getUser()))
                .refreshToken(refreshToken)
                .build();
    }


}
