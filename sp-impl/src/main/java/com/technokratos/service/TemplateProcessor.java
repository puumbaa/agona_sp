package com.technokratos.service;

import java.util.Map;

public interface TemplateProcessor {
    String process(Map<String, String> model);

}
