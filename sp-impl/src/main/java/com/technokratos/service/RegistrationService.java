package com.technokratos.service;

import com.technokratos.dto.request.RegistrationStartRequest;
import com.technokratos.dto.request.RegistrationCompleteRequest;

public interface RegistrationService {
    String startRegistration(RegistrationStartRequest data);
    String completeRegistration(RegistrationCompleteRequest data, String code);

}
