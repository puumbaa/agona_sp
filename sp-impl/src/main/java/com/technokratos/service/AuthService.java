package com.technokratos.service;


import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.response.TokenCoupleResponse;

public interface AuthService {

    TokenCoupleResponse login(LoginRequest data);
}
