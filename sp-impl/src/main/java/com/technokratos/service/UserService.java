package com.technokratos.service;


import com.technokratos.dto.*;
import com.technokratos.dto.request.EditAcademyGroupRequest;
import com.technokratos.dto.request.EditUserPasswordRequest;
import com.technokratos.dto.request.EditUserPhotoRequest;
import com.technokratos.dto.request.EditUserRequest;
import com.technokratos.dto.response.EditAcademyGroupResponse;
import com.technokratos.dto.response.UserResponse;
import org.springframework.core.io.Resource;

import java.util.UUID;

public interface UserService {
    UserResponse getById(UUID id);

    void setPhoto(EditUserPhotoRequest photo, UUID userId);

    Resource getPhoto(UUID userId);


    UserResponse updateUser(UUID id, EditUserRequest updated);

    EditAcademyGroupResponse updateAcademyGroup(UUID userId, EditAcademyGroupRequest academyGroup);

    String updatePassword(UUID id, EditUserPasswordRequest updated);

    CvDto updateUserCv(UUID id, CvDto updated);

}
