package com.technokratos.service;

import com.technokratos.dto.response.PracticalWorkBriefResponse;
import com.technokratos.dto.response.PracticalWorkResponse;

import java.util.List;
import java.util.UUID;

public interface PracticalWorkService {
    List<PracticalWorkBriefResponse> findAllByCourseId(UUID courseId);
    PracticalWorkResponse findById(UUID id);
}
