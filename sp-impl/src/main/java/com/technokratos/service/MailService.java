package com.technokratos.service;

import java.util.Map;

public interface MailService {
    void sendMail(String subject, String to, Map<String, String> model);
}
