package com.technokratos.service;

import com.technokratos.dto.response.TokenCoupleResponse;
import com.technokratos.model.RefreshToken;
import com.technokratos.model.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.UUID;

public interface JwtService {
    String getTokenFromAuthorizationHeader(String header);

    UserDetails getUserByToken(String token);

    String generateAccessToken(User user);

    UUID generateRefreshToken(User user);

    TokenCoupleResponse generateTokenCouple(User user);

    RefreshToken getRefreshToken(UUID refreshToken);

    void deleteRefreshToken(UUID refreshToken);

    TokenCoupleResponse refresh(UUID refreshToken);
}
