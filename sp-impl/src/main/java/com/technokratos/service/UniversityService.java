package com.technokratos.service;

import com.technokratos.dto.response.UniversityResponse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UniversityService {
    List<UniversityResponse> search(String type);
}
