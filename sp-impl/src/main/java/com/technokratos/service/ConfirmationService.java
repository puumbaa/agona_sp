package com.technokratos.service;

import com.technokratos.model.Confirmation;
import com.technokratos.model.User;

public interface ConfirmationService {
    Confirmation getConfirmation(String code);
    void delete(Confirmation confirmation);
    Confirmation createForUser(User user);
}
