package com.technokratos.service;

import com.technokratos.dto.enums.FileType;
import com.technokratos.model.FileInfo;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface FileService {
    Resource download(UUID id);

    UUID upload(MultipartFile file, FileType type);

    FileInfo getFileInfoById(UUID id);

    void deleteById(UUID id);

}
