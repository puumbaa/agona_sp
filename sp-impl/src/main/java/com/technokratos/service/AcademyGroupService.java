package com.technokratos.service;

import com.technokratos.dto.enums.UniversityYear;
import com.technokratos.dto.response.AcademyGroupResponse;
import com.technokratos.model.AcademyGroup;

import java.util.List;
import java.util.UUID;

public interface AcademyGroupService {
    AcademyGroup findById(UUID id);

    List<AcademyGroupResponse> search(UUID universityId, UniversityYear universityYear, String query);

}
