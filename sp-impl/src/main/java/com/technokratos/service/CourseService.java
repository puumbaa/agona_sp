package com.technokratos.service;

import com.technokratos.dto.response.CourseResponse;

import java.util.List;
import java.util.UUID;

public interface CourseService {
    List<CourseResponse> findAll();
    CourseResponse findById(UUID id);

    List<CourseResponse> getAvailableCourses(UUID user);
}
