package com.technokratos.security.details;

import com.technokratos.model.User;
import com.technokratos.repository.UserRepository;
import com.technokratos.security.details.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userCandidate = userRepository.findByEmail(username);

        if (userCandidate.isPresent()) {
            User user = userCandidate.get();
            log.debug("User - {} found in the database", user.getEmail());
            return UserDetailsImpl.builder()
                    .email(username)
                    .password(user.getPassword())
                    .role(user.getRole())
                    .state(user.getState())
                    .build();
        } else {
            log.error("User not found in the database");
            throw new UsernameNotFoundException("User not found");
        }
    }
}
