package com.technokratos.controllers;

import com.technokratos.api.UserApi;
import com.technokratos.dto.*;
import com.technokratos.dto.request.EditAcademyGroupRequest;
import com.technokratos.dto.request.EditUserPasswordRequest;
import com.technokratos.dto.request.EditUserPhotoRequest;
import com.technokratos.dto.request.EditUserRequest;
import com.technokratos.dto.response.CourseResponse;
import com.technokratos.dto.response.EditAcademyGroupResponse;
import com.technokratos.dto.response.UserResponse;
import com.technokratos.service.CourseService;
import com.technokratos.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;
    private final CourseService courseService;

    @Override
    public ResponseEntity<UserResponse> getUserById(UUID id) {
        return ResponseEntity.ok(userService.getById(id));
    }

    @Override
    public ResponseEntity<Resource> getUserPhoto(UUID id) {
        return ResponseEntity.ok(userService.getPhoto(id));
    }

    @Override
    public List<CourseResponse> getUserAvailableCourses(UUID id) {
        return courseService.getAvailableCourses(id);
    }

    @Override
    public void editUserPhoto(UUID id, EditUserPhotoRequest userPhotoId) {
        userService.setPhoto(userPhotoId, id);
    }

    @Override
    public ResponseEntity<EditAcademyGroupResponse> editAcademyGroup(UUID id, EditAcademyGroupRequest academyGroup) {
        return ResponseEntity.ok(userService.updateAcademyGroup(id, academyGroup));
    }

    @Override
    public ResponseEntity<UserResponse> editUser(UUID id, EditUserRequest updated) {
        return ResponseEntity.ok(userService.updateUser(id, updated));
    }

    @Override
    public ResponseEntity<String> editUserPassword(UUID id, EditUserPasswordRequest updated) {
        return ResponseEntity.ok(userService.updatePassword(id, updated));
    }



    @Override
    public ResponseEntity<CvDto> editCv(UUID id, @RequestBody CvDto updated) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(userService.updateUserCv(id, updated));
    }


}
