package com.technokratos.controllers;


import com.technokratos.api.RefreshTokenApi;
import com.technokratos.dto.response.TokenCoupleResponse;
import com.technokratos.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class JwtController implements RefreshTokenApi {

    private final JwtService jwtService;

    @Override
    public TokenCoupleResponse refreshToken(UUID refreshToken) {
        return jwtService.refresh(refreshToken);
    }
}
