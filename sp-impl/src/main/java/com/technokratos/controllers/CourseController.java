package com.technokratos.controllers;

import com.technokratos.api.CourseApi;
import com.technokratos.dto.request.CourseRequest;
import com.technokratos.dto.response.CourseResponse;
import com.technokratos.dto.response.PracticalWorkResponse;
import com.technokratos.dto.response.PracticalWorkBriefResponse;
import com.technokratos.service.CourseService;
import com.technokratos.service.PracticalWorkService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CourseController implements CourseApi {

    private final CourseService courseService;
    private final PracticalWorkService workService;

    @Override
    public List<CourseResponse> getAllCourses() {
        return courseService.findAll();
    }

    @Override
    public ResponseEntity<CourseResponse> getCourseById(UUID id) {
        return ResponseEntity.ok(courseService.findById(id));
    }

    @Override
    public List<PracticalWorkBriefResponse> getPracticalWorks(UUID id) {
        return workService.findAllByCourseId(id);
    }

    @Override
    public PracticalWorkResponse getPracticalWorkById(UUID workId) {
        return workService.findById(workId);
    }

    @Override
    public ResponseEntity<CourseResponse> addCourse(CourseRequest direction) {
        throw new NotImplementedException();

    }

    @Override
    public ResponseEntity<CourseResponse> editCourse(UUID id, CourseRequest directionForm) {
        throw new NotImplementedException();

    }

    @Override
    public ResponseEntity<String> removeCourseById(UUID id) {
        throw new NotImplementedException();

    }
}
