package com.technokratos.controllers;

import com.technokratos.api.RegistrationApi;
import com.technokratos.dto.request.RegistrationStartRequest;
import com.technokratos.dto.request.RegistrationCompleteRequest;
import com.technokratos.service.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RegistrationController implements RegistrationApi {

    private final RegistrationService registrationService;

    @Override
    public ResponseEntity<String> startRegistration(RegistrationStartRequest data) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(registrationService.startRegistration(data));
    }

    @Override
    public ResponseEntity<String> completeRegistration(String code, RegistrationCompleteRequest data) {
        return ResponseEntity.ok(registrationService.completeRegistration(data, code));
    }
}
