package com.technokratos.controllers;

import com.technokratos.api.AuthApi;
import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.response.TokenCoupleResponse;
import com.technokratos.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController implements AuthApi {

    private final AuthService authService;



    @Override
    public ResponseEntity<TokenCoupleResponse> login(LoginRequest data) {
        return ResponseEntity.ok(authService.login(data));

    }
}
