package com.technokratos.controllers;

import com.technokratos.api.FileApi;
import com.technokratos.dto.response.FileInfoResponse;
import com.technokratos.dto.enums.FileType;
import com.technokratos.service.FileService;
import com.technokratos.utils.mappers.FileMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class FileController implements FileApi {

    private final FileService fileService;
    private final FileMapper fileMapper;

    @Override
    public ResponseEntity<UUID> upload(MultipartFile file, FileType type) {
        return ResponseEntity.ok(fileService.upload(file, type));
    }

    @Override
    public ResponseEntity<Resource> downloadResource(UUID id) {
        return ResponseEntity.ok(fileService.download(id));
    }

    @Override
    public ResponseEntity<FileInfoResponse> download(UUID id) {
        return ResponseEntity.ok(fileMapper.toResponse(fileService.getFileInfoById(id)));
    }
}
