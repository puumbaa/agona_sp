package com.technokratos.controllers;

import com.technokratos.api.UniversityApi;
import com.technokratos.dto.enums.UniversityYear;
import com.technokratos.dto.response.AcademyGroupResponse;
import com.technokratos.dto.response.UniversityResponse;
import com.technokratos.service.AcademyGroupService;
import com.technokratos.service.UniversityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UniversityController implements UniversityApi {

    private final UniversityService universityService;
    private final AcademyGroupService academyGroupService;

    @Override
    public List<UniversityResponse> getAllUniversities(String query) {
        return universityService.search(query);
    }

    @Override
    public List<UniversityYear> getUniversityYears() {
        return List.of(UniversityYear.values());
    }

    @Override
    public List<AcademyGroupResponse> getAcademyGroup(UUID universityId, UniversityYear universityYear, String query) {
        return academyGroupService.search(universityId, universityYear, query);
    }
}
