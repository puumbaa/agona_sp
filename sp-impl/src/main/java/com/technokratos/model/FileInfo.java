package com.technokratos.model;


import com.technokratos.dto.enums.FileType;
import com.technokratos.model.embeddable.FileMetaInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class FileInfo extends ParentEntity {

    @Enumerated(EnumType.STRING)
    private FileType fileType;
    @Embedded
    private FileMetaInfo metaInfo;

}
