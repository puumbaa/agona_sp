package com.technokratos.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Course  extends ParentEntity{

    private String name;
    private String description;
    @ManyToOne
    @JoinColumn(name = "university_id")
    private University university;
    @OneToMany(mappedBy = "course")
    private List<PracticalWork> practicalWorks;
}
