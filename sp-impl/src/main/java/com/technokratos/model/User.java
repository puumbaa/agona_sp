package com.technokratos.model;

import com.technokratos.dto.enums.Role;
import com.technokratos.dto.enums.State;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "account")
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = "cv")
public class User extends ParentEntity {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    @Enumerated(EnumType.STRING)
    private Role role;
    private String phone;
    private LocalDate birthdate;
    private String address;
    private String telegram;
    @ManyToOne
    @JoinColumn(name = "academy_group_id")
    private AcademyGroup academyGroup;
    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "acc_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id"),
                name = "user_course")
    private Set<Course> courses;
    @OneToOne
    @JoinColumn(name = "cv_id")
    private Cv cv;
    @OneToOne
    @JoinColumn(name = "photo_id")
    private FileInfo photo;
    @Enumerated(EnumType.STRING)
    private State state;


}


