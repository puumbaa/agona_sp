package com.technokratos.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
public class PracticalWork extends ParentEntity {
    private String title;
    private String generalInfo;
    private String technicalRequirements;
    @OneToMany
    @JoinTable(name = "practical_work_file",
            joinColumns = @JoinColumn(name = "practical_work_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "file_info_id", referencedColumnName = "id")
    )
    private Set<FileInfo> files;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    private Course course;
}
