package com.technokratos.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Cv extends ParentEntity {
    @OneToOne
    @JoinColumn(name = "cv_file_id")
    private FileInfo file;
    @ManyToMany
    @JoinTable(
            joinColumns = @JoinColumn(name = "cv_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id"),
            name = "cv_skill"
    )
    private Set<Skill> skills;
    private String experienceDescription;
    private String portfolioLinks;
    @OneToOne(mappedBy = "cv")
    private User user;
}
