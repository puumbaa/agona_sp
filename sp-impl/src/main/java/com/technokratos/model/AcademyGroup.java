package com.technokratos.model;

import com.technokratos.dto.enums.UniversityYear;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class AcademyGroup extends ParentEntity {

    private String name;

    @Enumerated(EnumType.STRING)
    private UniversityYear universityYear;
    @ManyToOne
    @JoinColumn(name = "university_id")
    private University university;

}
