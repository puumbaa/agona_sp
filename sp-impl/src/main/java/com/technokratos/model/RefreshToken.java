package com.technokratos.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Table(name = "refresh")
public class RefreshToken extends ParentEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "acc_id")
    private User user;
    @Column(name = "expired_at")
    private LocalDateTime expiredAt;



}
