package com.technokratos.model.embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class FileMetaInfo {
    private long size;
    private String mimeType;
    private String originalFileName;
    private String storageId;
}
