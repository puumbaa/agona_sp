package com.technokratos.repository;

import com.technokratos.model.Cv;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CvRepository extends JpaRepository<Cv, UUID> {
}
