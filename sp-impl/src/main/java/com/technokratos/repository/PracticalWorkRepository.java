package com.technokratos.repository;

import com.technokratos.model.PracticalWork;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PracticalWorkRepository extends JpaRepository<PracticalWork, UUID> {
    List<PracticalWork> findByCourse_Id(UUID courseId);
}
