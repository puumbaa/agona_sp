package com.technokratos.repository;

import com.technokratos.model.Course;
import com.technokratos.model.University;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface CourseRepository extends JpaRepository<Course, UUID> {
    List<Course> findByUniversity(University university);
}
