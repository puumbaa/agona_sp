package com.technokratos.repository;

import com.technokratos.model.University;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface UniversityRepository extends JpaRepository<University, UUID> {
    Page<University> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
}
