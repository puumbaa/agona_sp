package com.technokratos.repository;

import com.technokratos.model.FileInfo;
import com.technokratos.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    @Modifying
    @Query("update User u set u.photo = :photoId where u.id = :userId")
    void setPhotoId(@Param("photoId") FileInfo photoId, @Param("userId") UUID userId);

    Optional<User> findByEmail(String username);

    boolean existsByEmail(String email);
}
