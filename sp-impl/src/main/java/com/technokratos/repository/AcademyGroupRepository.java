package com.technokratos.repository;

import com.technokratos.dto.enums.UniversityYear;
import com.technokratos.model.AcademyGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AcademyGroupRepository extends JpaRepository<AcademyGroup, UUID> {
    Page<AcademyGroup> findAllByUniversity_IdAndUniversityYearAndNameContainingIgnoreCase(
            UUID universityId,
            UniversityYear universityYear,
            String query,
            Pageable pageable);
}
