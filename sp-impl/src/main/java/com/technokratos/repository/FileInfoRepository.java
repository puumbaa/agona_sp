package com.technokratos.repository;

import com.technokratos.model.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface FileInfoRepository extends JpaRepository<FileInfo, UUID> {


}
