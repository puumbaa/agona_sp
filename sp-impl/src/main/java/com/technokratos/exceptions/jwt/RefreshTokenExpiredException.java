package com.technokratos.exceptions.jwt;

import com.technokratos.exceptions.AgonaServiceException;
import com.technokratos.model.RefreshToken;
import org.springframework.http.HttpStatus;

public class RefreshTokenExpiredException extends AgonaServiceException {
    public RefreshTokenExpiredException() {
        super(HttpStatus.UNAUTHORIZED, "Срок действия токена обновления истек");
    }
}
