package com.technokratos.exceptions.jwt;

import com.technokratos.exceptions.AgonaServiceException;
import org.springframework.http.HttpStatus;

public class WrongTokenFormatException extends AgonaServiceException {
    public WrongTokenFormatException() {
        super(HttpStatus.UNAUTHORIZED, "Невалидный токен авторизации");
    }
}
