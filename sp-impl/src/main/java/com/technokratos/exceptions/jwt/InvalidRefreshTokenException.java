package com.technokratos.exceptions.jwt;

import com.technokratos.exceptions.AgonaServiceException;
import org.springframework.http.HttpStatus;

public class InvalidRefreshTokenException extends AgonaServiceException {

    public InvalidRefreshTokenException() {
        super(HttpStatus.UNAUTHORIZED, "Токен обновления невалидный");
    }
}
