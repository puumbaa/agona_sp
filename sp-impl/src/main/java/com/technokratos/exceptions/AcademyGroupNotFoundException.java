package com.technokratos.exceptions;

public class AcademyGroupNotFoundException extends AgonaNotFoundException{
    public AcademyGroupNotFoundException() {
        super("Академическая группа не найдена. Проверьте правильность введенных данных");
    }
}
