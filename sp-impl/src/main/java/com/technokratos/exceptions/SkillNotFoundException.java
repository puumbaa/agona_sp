package com.technokratos.exceptions;

public class SkillNotFoundException extends AgonaNotFoundException {
    public SkillNotFoundException() {
        super("Skill not found");
    }
}
