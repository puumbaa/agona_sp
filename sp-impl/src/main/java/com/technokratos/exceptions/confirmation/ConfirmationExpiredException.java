package com.technokratos.exceptions.confirmation;

import com.technokratos.exceptions.AgonaServiceException;
import org.springframework.http.HttpStatus;

public class ConfirmationExpiredException extends AgonaServiceException {

    public ConfirmationExpiredException() {
        super(HttpStatus.FORBIDDEN, "Истек срок действия ссылки");
    }
}
