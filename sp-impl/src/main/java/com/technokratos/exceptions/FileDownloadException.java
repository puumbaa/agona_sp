package com.technokratos.exceptions;

import org.springframework.http.HttpStatus;

public class FileDownloadException extends AgonaServiceException {
    public FileDownloadException() {
        super(HttpStatus.BAD_REQUEST, "Не удалось получить доступ к ресурсам файла");
    }
}
