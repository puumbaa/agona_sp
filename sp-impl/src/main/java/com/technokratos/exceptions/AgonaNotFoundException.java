package com.technokratos.exceptions;

import org.springframework.http.HttpStatus;

public class AgonaNotFoundException extends AgonaServiceException{
    public AgonaNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
