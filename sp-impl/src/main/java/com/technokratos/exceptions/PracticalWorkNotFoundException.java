package com.technokratos.exceptions;

public class PracticalWorkNotFoundException extends AgonaNotFoundException{
    public PracticalWorkNotFoundException() {
        super("Практическая работа не найдена");
    }
}
