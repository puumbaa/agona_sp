package com.technokratos.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AgonaServiceException extends RuntimeException {
    private final HttpStatus status;
    private final String message;

    public AgonaServiceException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
