package com.technokratos.exceptions.not_found;

import com.technokratos.exceptions.AgonaNotFoundException;

public class FileInformationNotFoundException extends AgonaNotFoundException {

    public FileInformationNotFoundException() {
        super("Файл не найден. Убедитесь, что ввели верный уникальный идентификатор");
    }
}
