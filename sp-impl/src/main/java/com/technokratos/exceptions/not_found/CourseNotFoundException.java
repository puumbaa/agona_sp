package com.technokratos.exceptions.not_found;

import com.technokratos.exceptions.AgonaNotFoundException;

public class CourseNotFoundException extends AgonaNotFoundException {
    public CourseNotFoundException() {
        super("Курс не найден");
    }
}
