package com.technokratos.exceptions.not_found;


import com.technokratos.exceptions.AgonaNotFoundException;

public class UserNotFoundException extends AgonaNotFoundException {

    public UserNotFoundException() {
        super("Пользователь не найден. Убедитесь, что ввели верный уникальный идентификатор");
    }
}
