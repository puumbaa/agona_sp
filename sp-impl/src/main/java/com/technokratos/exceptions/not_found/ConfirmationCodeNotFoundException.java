package com.technokratos.exceptions.not_found;

import com.technokratos.exceptions.AgonaNotFoundException;

public class ConfirmationCodeNotFoundException extends AgonaNotFoundException {

    public ConfirmationCodeNotFoundException() {
        super("Код подтверждения не найден.");
    }

}
