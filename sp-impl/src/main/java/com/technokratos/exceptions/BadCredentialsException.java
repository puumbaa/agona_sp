package com.technokratos.exceptions;

import org.springframework.http.HttpStatus;

public class BadCredentialsException extends AgonaServiceException {
    public BadCredentialsException(String msg) {
        super(HttpStatus.FORBIDDEN, msg);
    }
}
