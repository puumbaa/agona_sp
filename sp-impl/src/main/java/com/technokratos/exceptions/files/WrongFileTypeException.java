package com.technokratos.exceptions.files;

import com.technokratos.dto.enums.FileType;
import com.technokratos.exceptions.AgonaServiceException;
import org.springframework.http.HttpStatus;

public class WrongFileTypeException extends AgonaServiceException {

    public WrongFileTypeException(FileType expected, FileType received) {
        super(HttpStatus.BAD_REQUEST,
                String.format("Неверный тип файла. Ожидалось - %s, получено - %s",
                        expected.getDescription(), received.getDescription()));
    }
}
