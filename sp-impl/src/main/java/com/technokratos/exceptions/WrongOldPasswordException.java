package com.technokratos.exceptions;

import org.springframework.http.HttpStatus;

public class WrongOldPasswordException extends AgonaServiceException {
    public WrongOldPasswordException() {
        super(HttpStatus.FORBIDDEN, "Введен неверный старый пароль");
    }
}
