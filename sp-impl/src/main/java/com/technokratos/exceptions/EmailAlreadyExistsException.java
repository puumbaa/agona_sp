package com.technokratos.exceptions;

import org.springframework.http.HttpStatus;

public class EmailAlreadyExistsException extends AgonaServiceException {
    public EmailAlreadyExistsException() {
        super(HttpStatus.FORBIDDEN, "Аккаунт с такой почтой уже существует");
    }
}
