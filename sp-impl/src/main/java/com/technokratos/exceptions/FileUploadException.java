package com.technokratos.exceptions;

import org.springframework.http.HttpStatus;

public class FileUploadException extends AgonaServiceException {
    public FileUploadException() {
        super(HttpStatus.BAD_REQUEST, "Ошибка загрузки файла; Файл не удалось загрузить. Повторите попытку еще раз");
    }
}
