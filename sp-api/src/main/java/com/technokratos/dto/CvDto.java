package com.technokratos.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.technokratos.dto.response.SkillResponse;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "CV пользователя")
public class CvDto {
    @JsonProperty("cv_file_id")
    @Schema(description = "Уникальный идентификатор cv-файла", example = "GUID")
    private UUID cvFileId;
    @Schema(description = "Навыки", implementation = SkillResponse.class)
    private Set<SkillResponse> skills;
    @JsonProperty("experience_desc")
    @Schema(description = "Описание опыта",
            example = "I’m a senior Java Developer with 15 " +
                    "years of experience working in financial services. " +
                    "I have been a part of building the mobile banking " +
                    "infrastructure that everyone has come to know and " +
                    "love as well as working with online trading. " +
                    "I’m looking to expand my career and " +
                    "I’m very interested in your company.",
            maxLength = 1000)
    @Size(max = 1000)
    private String experienceDescription;
    @JsonProperty("portfolio_links")
    @Schema(description = "Ссылки на резюме, разделенные запятой",
            example = "https://github.com/john_doe, " + "https://career.habr.com/john_doe",
            maxLength = 2048)
    @Size(max = 2048)
    private String portfolioLinks;

}
