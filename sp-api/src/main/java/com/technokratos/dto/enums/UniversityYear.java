package com.technokratos.dto.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum UniversityYear {
    FIRST_BACHELOR("Первый курс (бакалавриат)"),
    SECOND_BACHELOR("Второй курс (бакалавриат)"),
    THIRD_BACHELOR("Третий курс (бакалавриат)"),
    FOURTH_BACHELOR("Четвертый курс (бакалавриат)"),
    FIRST_MASTER("Первый курс (Магистратура)"),
    SECOND_MASTER("Второй курс (Магистратура)");

    private final String translate;
}
