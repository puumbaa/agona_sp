package com.technokratos.dto.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum FileType {
    USER_PHOTO("Фото пользователя"), USER_CV("CV пользователя");
    private final String description;
}
