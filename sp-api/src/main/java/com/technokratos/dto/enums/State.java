package com.technokratos.dto.enums;

public enum State {
    CONFIRMED, NOT_CONFIRMED, BANNED, DELETED;
}