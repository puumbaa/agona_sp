package com.technokratos.dto.enums;

public enum Role {
    ADMIN, MANAGER, USER
}
