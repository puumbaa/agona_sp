package com.technokratos.dto.response;

import com.technokratos.dto.enums.FileType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class FileInfoBriefResponse {
    private FileType fileType;
    private long size;
}
