package com.technokratos.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Навыки пользователя")
public class SkillResponse {
    @Schema(description = "Название навыка", example = "Java")
    private String name;
}
