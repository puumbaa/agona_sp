package com.technokratos.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.technokratos.dto.CvDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Пользователь")
public class UserResponse {
    @Schema(description = "Уникальный идентификатор", example = "GUID")
    private UUID id;
    @JsonProperty("first_name")
    @Schema(description = "Имя", example = "Эльнур")
    private String firstName;
    @JsonProperty("last_name")
    @Schema(description = "Фамилия", example = "Сардаров")
    private String lastName;
    @Schema(description = "Телефонный номер", example = "89044874258", pattern = "^[0-9]{11}$", minLength = 11)
    private String phone;
    @Schema(description = "Дата рождения", example = "2002-07-15")
    private LocalDate birthdate;
    @Schema(description = "Адрес проживания", example = "Казань, Деревня Универсиады")
    private String address;
    @Schema(description = "Никнейм в телеграмме", example = "@puumbaa")
    private String telegram;
    @Schema(description = "Университет", implementation = UniversityResponse.class)
    private UniversityResponse university;
    @JsonProperty("academy_group")
    @Schema(description = "Академическая группа", implementation = AcademyGroupResponse.class)
    private AcademyGroupResponse academyGroup;
    @Schema(description = "Список курсов", implementation = CourseResponse.class)
    private Set<CourseResponse> courses;
    @Schema(description = "CV пользователя", implementation = CvDto.class)
    private CvDto cv;
}
