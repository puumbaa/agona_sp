package com.technokratos.dto.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class PracticalWorkResponse extends PracticalWorkBriefResponse {
    @JsonProperty("general_info")
    private String generalInfo;
    @JsonProperty("technicalRequirements")
    private String technicalRequirements;
    private Set<FileInfoBriefResponse> files;
}
