package com.technokratos.dto.request;

import com.technokratos.validation.SamePasswords;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@SamePasswords(passwordRepeatFieldName = "passwordConfirmation")
@Schema(description = "Форма завершения регистрации")
public class RegistrationCompleteRequest {

    @Schema(description =
            "Пароль. Должен быть не менее 8 символов, " +
                    "содержать как минимум одну заглавную букву, " +
                    "цифру и спец.символ (!@#$&*).",
            example = "QweQwe1!", minLength = 8,
            pattern = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$")
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$")
    @NotBlank(message = "New password must be specified")
    private String password;
    @Schema(description = "Повтор пароля", example = "QweQwe1!")
    @NotBlank(message = "Repeat password must be specified")
    private String passwordConfirmation;
    // todo: add university & course
}
