package com.technokratos.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EditUserRequest {

    @Size(min = 2, max = 255, message = "First name length should be between {min} - {max}")
    @NotBlank(message = "First name should not be empty")
    private String firstName;
    @Size(min = 2, max = 255, message = "Last name length should be between {min} - {max}")
    @NotBlank(message = "Last name should not be empty")
    private String lastName;
    // todo: validation for phone number using google lib ?
    @NotBlank(message = "Phone number should not be empty")
    @Pattern(regexp = "^[0-9]{11}$")
    private String phone;
    @Past
    @NotBlank(message = "Birthdate should not be empty")
    private LocalDate birthdate;
    @NotBlank(message = "Address should not be empty")
    private String address;
    @NotBlank(message = "Telegram nickname should not be empty")
    private String telegram;
}
