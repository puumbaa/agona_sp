package com.technokratos.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Форма регистрации")
public class RegistrationStartRequest {
    @Size(min = 2, max = 255, message = "First name length should be between {min} - {max} characters")
    @NotBlank(message = "First name should not be empty")
    @Schema(description = "Имя", example = "Эльнур")
    private String firstName;
    @Size(min = 2, max = 255, message = "Last name length should be between {min} - {max} characters")
    @NotBlank(message = "Last name should not be empty")
    @Schema(description = "Фамилия", example = "Сардаров")
    private String lastName;
    @Email
    @NotBlank(message = "Email should not be empty")
    @Schema(description = "Электронная почта", example = "sardarovelnur@gmail.com")
    private String email;
}
