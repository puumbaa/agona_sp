package com.technokratos.validation;

import org.springframework.util.ReflectionUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

public class CredentialsValidator implements ConstraintValidator<SamePasswords, Object> {

    private String passwordFieldName;
    private String passwordRepeatFieldName;


    @Override
    public void initialize(SamePasswords constraintAnnotation) {
        passwordFieldName = constraintAnnotation.passwordFieldName();
        passwordRepeatFieldName = constraintAnnotation.passwordRepeatFieldName();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        Field passwordField = ReflectionUtils.findField(object.getClass(), passwordFieldName);
        Field passwordRepeatField = ReflectionUtils.findField(object.getClass(), passwordRepeatFieldName);

        if (passwordField != null && passwordRepeatField != null) {
            try {
                passwordField.setAccessible(true);
                passwordRepeatField.setAccessible(true);
                String password = (String) passwordField.get(object);
                String passwordRepeat = (String) passwordRepeatField.get(object);
                return password.equals(passwordRepeat);
            } catch (IllegalAccessException e) {
                return false;
            }
        } else {
            throw new IllegalArgumentException("Wrong field names for passwords");
        }
    }
}
