package com.technokratos.validation;



import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CredentialsValidator.class)
public @interface SamePasswords {
    String passwordFieldName() default "password";

    String passwordRepeatFieldName() default "passwordRepeat";

    String message() default "Passwords are not same!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
