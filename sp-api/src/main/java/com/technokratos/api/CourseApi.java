package com.technokratos.api;

import com.technokratos.dto.request.CourseRequest;
import com.technokratos.dto.response.CourseResponse;
import com.technokratos.dto.response.PracticalWorkResponse;
import com.technokratos.dto.response.PracticalWorkBriefResponse;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RequestMapping("/api/course")
public interface CourseApi {

    @GetMapping
    List<CourseResponse> getAllCourses();

    @GetMapping("/{id}")
    ResponseEntity<CourseResponse> getCourseById(@PathVariable UUID id);

    @GetMapping("/{id}/work")
    List<PracticalWorkBriefResponse> getPracticalWorks(@PathVariable UUID id);

    @GetMapping("/{id}/work/{work_id}")
    PracticalWorkResponse getPracticalWorkById(@PathVariable("work_id") UUID workId);

    @PostMapping
    ResponseEntity<CourseResponse> addCourse(@RequestBody CourseRequest direction);

    @PutMapping("/{id}")
    ResponseEntity<CourseResponse> editCourse(@PathVariable UUID id, @RequestBody CourseRequest directionForm);

    @DeleteMapping("/{id}")
    ResponseEntity<String> removeCourseById(@PathVariable UUID id);


}
