package com.technokratos.api;

import com.technokratos.dto.response.FileInfoResponse;
import com.technokratos.dto.enums.FileType;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RequestMapping("/api/file")
public interface FileApi {

    @PostMapping
    ResponseEntity<UUID> upload(MultipartFile file, FileType type);

    @GetMapping("/{id}")
    ResponseEntity<Resource> downloadResource(@PathVariable UUID id);

    @GetMapping("/{id}/info")
    ResponseEntity<FileInfoResponse> download(@PathVariable UUID id);
}
