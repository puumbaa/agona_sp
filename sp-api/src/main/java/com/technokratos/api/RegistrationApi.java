package com.technokratos.api;

import com.technokratos.dto.request.RegistrationStartRequest;
import com.technokratos.dto.request.RegistrationCompleteRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping("/api/register")
public interface RegistrationApi {
    @PostMapping
    ResponseEntity<String> startRegistration(@Valid @RequestBody RegistrationStartRequest data);

    @PostMapping("/{code}")
    ResponseEntity<String> completeRegistration(
            @PathVariable(name = "code") String code,
            @RequestBody RegistrationCompleteRequest data
    );
}
