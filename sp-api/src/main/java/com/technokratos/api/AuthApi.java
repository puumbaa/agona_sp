package com.technokratos.api;

import com.technokratos.dto.request.LoginRequest;
import com.technokratos.dto.response.TokenCoupleResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/auth/")
public interface AuthApi {

    @PostMapping("/login")
    ResponseEntity<TokenCoupleResponse> login(@RequestBody LoginRequest data);

}
