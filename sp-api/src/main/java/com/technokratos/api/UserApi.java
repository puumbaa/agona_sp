package com.technokratos.api;

import com.technokratos.dto.*;
import com.technokratos.dto.request.EditAcademyGroupRequest;
import com.technokratos.dto.request.EditUserPasswordRequest;
import com.technokratos.dto.request.EditUserPhotoRequest;
import com.technokratos.dto.request.EditUserRequest;
import com.technokratos.dto.response.CourseResponse;
import com.technokratos.dto.response.EditAcademyGroupResponse;
import com.technokratos.dto.response.UserResponse;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping("/api/user")
public interface UserApi {

    @GetMapping(value = "/{id}")
    ResponseEntity<UserResponse> getUserById(@PathVariable UUID id);

    @GetMapping("/{id}/photo")
    ResponseEntity<Resource> getUserPhoto(@PathVariable UUID id);

    @GetMapping("/{id}/course")
    List<CourseResponse> getUserAvailableCourses(@PathVariable UUID id);

    @PutMapping("/{id}/photo")
    void editUserPhoto(@PathVariable UUID id, @RequestBody EditUserPhotoRequest file);

    @PutMapping("/{id}/university")
    ResponseEntity<EditAcademyGroupResponse> editAcademyGroup(@PathVariable UUID id, @Valid @RequestBody EditAcademyGroupRequest academyGroup);

    @PutMapping("/{id}")
    ResponseEntity<UserResponse> editUser(@PathVariable UUID id, @Valid @RequestBody EditUserRequest updated);

    @PutMapping("/{id}/password")
    ResponseEntity<String> editUserPassword(@PathVariable UUID id, @Valid @RequestBody EditUserPasswordRequest updated);

    @PutMapping("/{id}/cv")
    ResponseEntity<CvDto> editCv(@PathVariable UUID id, @Valid @RequestBody CvDto updated);
}
