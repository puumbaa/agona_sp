package com.technokratos.api;

import com.technokratos.dto.response.TokenCoupleResponse;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("/api/token/refresh")
public interface RefreshTokenApi {

    @PostMapping
    TokenCoupleResponse refreshToken(@RequestParam(name = "refreshToken") UUID refreshToken);
}
