package com.technokratos.api;

import com.technokratos.dto.enums.UniversityYear;
import com.technokratos.dto.response.AcademyGroupResponse;
import com.technokratos.dto.response.UniversityResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.UUID;

@RequestMapping("/api/university")
public interface UniversityApi {

    @GetMapping
    List<UniversityResponse> getAllUniversities(@RequestParam(required = false) String query);

    @GetMapping("/years")
    List<UniversityYear> getUniversityYears();

    @GetMapping("/{id}/academy_group")
    List<AcademyGroupResponse> getAcademyGroup(@PathVariable("id") UUID universityId,
                                               @RequestParam(name = "year") UniversityYear universityYear,
                                               @RequestParam(required = false) String query);

}
