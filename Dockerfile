FROM gradle:jdk11-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle clean bootJar --no-daemon

FROM openjdk:11-jre-slim
COPY --from=build /home/gradle/src/sp-impl/build/libs/*.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]

EXPOSE 80
