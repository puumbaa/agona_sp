# Agona
#### О проекте

Сервис для предоставления образовательных курсов в сфере it студентам высших учебных заведений.
___

#### Как запустить проект? 
Для запуска проекта понадобится заранее установленный Docker на вашем устройстве. 

1. Выполните команды
   - `git clone https://gitlab.com/puumbaa/agona_sp.git`
   - `cd agona_sp`


2. В файле docker-compose.yaml измените некоторые свойства

```yaml
  mongodb:
    image: mongo
    container_name: mongodb
    volumes:
      - mongodata:/data
    ports:
      - 27017:27017
    networks:
      - agona_net
    environment:
      - MONGO_INITDB_ROOT_USERNAME=rootuser # Ваше имя пользователя 
      - MONGO_INITDB_ROOT_PASSWORD=rootpass # Ваш пароль
    
    
  application:
    build: .
    container_name: application
    ports:
      - 80:80
    networks:
      - agona_net
    depends_on:
      - mongodb
      - postgresql
    environment:
      - "SPRING_DATASOURCE_URL=jdbc:postgresql://database:5432/app_db"
      - "SPRING_DATASOURCE_USERNAME=postgres" 
      - "SPRING_DATASOURCE_PASSWORD=" # Ваш пароль от PostgreSQL
      - "MONGO_USERNAME=rootuser" # Ваше имя пользователя
      - "MONGO_PASSWORD=rootpass" # Ваш пароль
      - "MONGO_DATABASE=files-storage"
      - "MONGO_PORT=27017"
      - "MONGO_HOST=mongodb"
      - "MONGO_AUTHENTICATION_DATABASE=admin"
        
        
  postgresql:
    image: postgres
    container_name: database
    ports:
      - 5432:5432
    networks:
      - agona_net
    environment:
      POSTGRES_DB: app_db # Название базы данных 
      POSTGRES_USER: postgres # Ваше имя пользователя
      POSTGRES_PASSWORD: # Ваш пароль
```
3. Выполните команды

    - `docker-compose build`
    - `docker-compose up`

Теперь приложение доступно по адресу `http://localhost`
___ 
#### Документация
Вы можете ознакомиться с API сервиса по адресу `http://localhost/swagger-ui.html`
